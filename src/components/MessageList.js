import { useState } from "react";
import Message from "./Message";
import Modal from "./Modal"
import MessagesService from "../MessagesService";

const MessageList = ({messages,dispatchMessages}) => {
    const  messagesService  =  new  MessagesService();


    const [currentMessageId, setCurrentMessageId] = useState(null)
    const [showModal,setShowModal] = useState(false)

    const handleMessageDeleteClick = (id) => {
        setCurrentMessageId(id)
        setShowModal(true)
    }
    const handleMessageDeleteConfirm = (token) => {
        setShowModal(false)
        console.log("deleted confirm"+currentMessageId+" "+token)

        messagesService
        .deleteMessage(currentMessageId,token)
        .then(() => {console.log("hererere ?");dispatchMessages({type:"DELETE_MESSAGE",payload:currentMessageId})})
        .catch(error => {
            if( error.response ){
                console.log(error.response.data); // => the response payload 
                alert(error.response.data.message)
            }
        })
        
    }
    return (
        <>
        <div >
            {messages.map(message => (
                <Message 
                    key={message.id}
                    message={message} 
                    onDeleteClick={handleMessageDeleteClick} />
            ))}

        </div>
        <Modal 
            onConfirm={handleMessageDeleteConfirm} 
            showModal={showModal} 
            setShowModal ={setShowModal} />
        </>
    )

}

export default MessageList;