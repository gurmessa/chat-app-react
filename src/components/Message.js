
import moment from 'moment';
const Message = ({message,onDeleteClick}) => {
    const handleDeleteClick = () => {
        onDeleteClick(message.id)
    }
    const time = moment(message.created_at).format('MMM Do YY, h:mm:ss a');
    return (
        <div className="bg-white rounded-lg p-5 my-2 shadow-md">
        <div className="flex justify-between">
          <div className="font-light text-gray-600">{time}</div>
          <button className="text-gray-700 border-red-400 hover:text-white hover:bg-red-500 border-2 rounded-md px-3 py-1 text-xs"
                onClick={handleDeleteClick}
          >
              Delete
          </button>
        </div>

        <div className="text-xl font-bold text-gray-70">~anonymous</div>
        <p className="mt-2 text-gray-600">
          {message.text}
        </p>
      </div>


    )
}


export default Message;