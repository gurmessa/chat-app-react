import { useState } from "react"
import Modal from './Modal'
import MessagesService from "../MessagesService";

const AddMessage = ({dispatchMessages}) => {
    const  messagesService  =  new  MessagesService();

    //const inputTextArea = useRef(); 
    const [text,setText] =useState("")
    const handlePost = () => {
        //dispatchMessages({type:"ADD_MESSAGE",payload:{text:inputTextArea.current.value}})

        messagesService
        .addMessage(text)
        .then((result) => {
            setText("")
            dispatchMessages({type:"ADD_MESSAGE",payload:result.data})
        })
        .catch(error => {
            if( error.response ){
                console.log(error.response.data); // => the response payload 
                alert(error.response.data.message)
            }
        })
        
        
    }

    const [showModal,setShowModal] = useState(false)
    const handleDeleteAllClick = () => {
        setShowModal(true)
    }
    const handleDeleteAllConfirm = (token) => {
        setShowModal(false)

        messagesService
        .deleteAllMessages(token)
        .then(() => {dispatchMessages({type:"DELETE_ALL_MESSAGES"})})
        .catch(error => {
            if( error.response ){
                console.log(error.response.data); // => the response payload 
                alert(error.response.data.message)
            }
        })

    }

    return (
        <div className="bg-white rounded-lg p-5 my-2 shadow-md w-full flex flex-col">
            <textarea class="description bg-gray-100 sec p-3 h-32 border border-gray-300 outline-none" 
            placeholder="Type something in the box below"
            onChange={(e)=>{setText(e.target.value)}}
            value={text}
            >
            </textarea>

            <div class="buttons flex my-4">
                <button onClick={handlePost} disabled={text.length === 0}class="btn rounded-md border disabled:bg-blue-400 p-1 px-4 font-semibold cursor-pointer text-gray-200  bg-blue-500 hover:bg-blue-600">Post</button>
                <div onClick={handleDeleteAllClick} class="btn rounded-md border bg-red-500 hover:bg-red-600 p-1 px-4 font-semibold cursor-pointer text-gray-100 ml-auto">Delete All</div>
            </div>


            <Modal 
            onConfirm={handleDeleteAllConfirm} 
            showModal={showModal} 
            setShowModal ={setShowModal} />
            
        </div>

    )

    
}

export default AddMessage