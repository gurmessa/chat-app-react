import React,{ useState } from "react";

const Modal = ({onConfirm,showModal,setShowModal}) => {
    const [token,setToken] = useState("")
    return (
        <>
        {showModal ? (
          <>
            <div
              className=" justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
            >
            

              <div className="relative  my-6 mx-auto max-w-3xl">

                {/*content*/}
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                  {/*header*/}
                  <div className="flex items-start justify-between p-3   border-b border-solid border-blueGray-200 rounded-t">
                    <h3 className="text-xl font-semibold px-3">
                      Enter token to delete
                    </h3>
                    <button
                      className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                      onClick={() => setShowModal(false)}
                    >
                      <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                        ×
                      </span>
                    </button>
                  </div>
                  {/*body*/}
                  
                  <div className="relative p-6 flex-auto">
                    
                  <div className="w-full flex flex-col">
                    <input class="title bg-gray-100 border border-gray-300 p-2 mb-4 outline-none"  placeholder="Token" type="text"
                        onChange={(e)=>{setToken(e.target.value)}}
                    />
                  </div>


                  </div>
                  {/*footer*/}
                  <div className="flex items-center justify-end p-3 border-t border-solid border-blueGray-200 rounded-b">
                    <button
                      className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                      type="button"
                      onClick={() => setShowModal(false)}
                    >
                      Close
                    </button>
                    <button
                      className="btn rounded-md border disabled:bg-red-400 bg-red-500 hover:bg-red-600 p-1 px-4 font-semibold cursor-pointer text-gray-100 ml-auto"
                      type="button"
                      onClick={()=>{onConfirm(token)}}
                      disabled={token.length === 0}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
          </>
        ) : null}
      </>
  
    )
}


export default Modal;