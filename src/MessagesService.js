import axios from "axios";
//const API_URL = "http://localhost:8000/api"
const API_URL = "https://my-django-chat-api.herokuapp.com/api"

export default class MessagesService{
    
    getMessages(){
        const url = `${API_URL}/messages`
        return axios.get(url)
    }
    addMessage(text){
        const url = `${API_URL}/messages`
        return axios.post(url,{text})
    }
    deleteMessage(id,token){
        const url = `${API_URL}/messages/delete/${id}`
        const instance = axios.create({
            baseURL: url,
            headers: {'Authorization': token}
          });

        return instance.delete(url)
    }
    deleteAllMessages(token){
        const url = `${API_URL}/messages/delete-all`
        const instance = axios.create({
            baseURL: url,
            headers: {'Authorization': token}
          });

        return instance.delete(url)
    }
}
