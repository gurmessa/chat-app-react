/* eslint-disable */
import React,{ useReducer,useEffect } from "react";
import MessageList from "./components/MessageList";
import AddMessage from "./components/AddMessage";
import MessagesService from "./MessagesService";

const messages_list = [{id:1,date:"10:01:04 AM",text:"Hello world"},{id:2,date:"10:01:04 AM",text:"Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempora expedita dicta totam aspernatur doloremque. Excepturi iste iusto eos enim reprehenderit nisi, accusamus delectus nihil quis facere in modi ratione libero!"}]
  
function App() {
  const  messagesService  =  new  MessagesService();

  const messagesReducer = (state, action) => {
    switch (action.type){
      case 'ADD_MESSAGE':
        return [action.payload,...state]
      case 'DELETE_MESSAGE':
        return state.filter(s => s.id !==action.payload)
      case 'DELETE_ALL_MESSAGES':
        return []
      case 'ADD_ALL':
        return action.payload
      default:
        return
    }
  }
  const [messages, dispatchMessages] = useReducer(messagesReducer,[])
  

  useEffect(()=>{
    messagesService
    .getMessages()
    .then((result) => {
      console.log(result)
      dispatchMessages({type:"ADD_ALL",payload:result.data})
    })
  },[])

  return (
    <div className="bg-gray-100 h-screen">

      <div className="max-w-4xl mx-auto py-5">
          <h3 className="text-3xl font-semibold my-5">Chatter</h3>
          <AddMessage dispatchMessages={dispatchMessages} />
          <MessageList dispatchMessages={dispatchMessages} messages={messages}/>
      </div>




    </div>
  );
}

export default App;
